SRC := list.c
INC := ./
CC := gcc

all:
	${CC} ${SRC} -o liblist.so -fPIC -shared
